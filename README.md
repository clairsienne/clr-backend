# Clairsienne REST API

Ce projet contient les sources de l'API REST de Clairsienne.
Cette API a pour vocation de centraliser les appels aux différents services utilisés dans le cadre de l'application "Moteur de recherche Patrimoine".
