import Dependencies._
import sbt.Keys.test

val meta = """META.INF(.)*""".r

lazy val commons = Seq(
  homepage := Some(url("https://gitlab.com/clairsienne/clr-backend")),
  scmInfo := Some(
    ScmInfo(
      url("https://gitlab.com/clairsienne/clr-backend/clr-backend.git"),
      "scm:git@gitlab.com/clairsienne/clr-backend/clr-backend.git"
    )
  )
)

lazy val models = (project in file("modules/models")).settings(
  commons,
  name := "clr-models",
  description := "Clairsienne models",
  homepage := Some(url("https://gitlab.com/clairsienne/clr-models")),
  libraryDependencies ++= Seq(
    playJson,
    akkaHttpSprayJson % Provided,
    akkaHttpPlayJson % Provided,
    rdf4jRuntime
  )
)

lazy val parsers = (project in file("modules/parsers")).settings(
  commons,
  name := "clr-parsers",
  description := "Clairsienne parsers",
  homepage := Some(url("https://gitlab.com/clairsienne/clr-parsers")),
  libraryDependencies ++= Seq(
    jodaTime,
    typesafeConfig,
    scalaLogging,
    logbackClassic % Test,
    akkaStream,
    akkaSlf4j,
    akkaHttp % Provided,
    akkaHttpSprayJson % Provided,
    akkaHttpPlayJson % Provided,
    scalaTest % Test,
    akkaTestkit % Test,
    akkaStreamTestkit % Test,
    poi,
    poiXml,
    poiXmlSchemas,
    jt400,
    postgresql,
    sqlJdbc42,
    ldap,
    rdf4jRuntime
  )
).dependsOn(models).aggregate(models)

lazy val apiclient = (project in file("modules/apiclient")).settings(
  commons,
  name := "clr-restapi-client",
  description := "Clairsienne REST API Client",
  libraryDependencies ++= Seq(
    typesafeConfig,
    scalaLogging,
    akkaHttp,
    akkaHttpSprayJson,
    akkaHttpPlayJson,
    scalaTest % Test,
    logbackClassic % Test,
    akkaHttpSprayJson,
    akkaHttpPlayJson,
    sttp,
    quartz
  )
).dependsOn(models).aggregate(models)

lazy val restapi = (project in file("modules/restapi")).settings(
  commons,
  name := "clr-restapi",
  unmanagedResourceDirectories in Compile += {
    baseDirectory.value / "src/main/resources"
  },
  libraryDependencies ++= Seq(
    logbackClassic,
    akkaHttp,
    akkaHttpSprayJson,
    akkaHttpPlayJson,
    akkaHttpXml,
    scalaTest % Test,
    akkaHttpTestKit % Test,
    akkaTestkit % Test,
    akkaStreamTestkit % Test
  ),
  test in assembly := {},
  assemblyMergeStrategy in assembly := {
    case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
    case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
    //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
    case n if n.endsWith(".conf") => MergeStrategy.concat
    case n if n.endsWith(".properties") => MergeStrategy.concat
    case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
    case PathList("META-INF", xs@_*) => MergeStrategy.discard
    case meta(_) => MergeStrategy.discard
    case x => MergeStrategy.first
  },
  mainClass in assembly := Some("com.clairsienne.com.restapi.RestApiServer"),
  assemblyJarName in assembly := "clr-restapi.jar",
  packageName in Docker := packageName.value,
  version in Docker := version.value,
  dockerExposedPorts ++= Seq(8080),
  dockerRepository := Some("registry.gitlab.com/clairsienne/clr-backend"),
).dependsOn(parsers).aggregate(parsers)
  .enablePlugins(JavaAppPackaging)
  .enablePlugins(DockerPlugin)
  .enablePlugins(AshScriptPlugin)