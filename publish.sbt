import sbt.url

ThisBuild / scalaVersion := "2.12.10"
ThisBuild / organization := "com.clairsienne"
ThisBuild / organizationName := "Clairsienne"

ThisBuild / developers := List(
  Developer(
    id = "ndelaforge",
    name = "Nicolas Delaforge",
    email = "nicolas.delaforge@mnemotix.com",
    url = url("http://www.mnemotix.com")
  )
)
ThisBuild / credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
ThisBuild / updateOptions := updateOptions.value.withGigahorse(false)
// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }
ThisBuild / publishArtifact := true
ThisBuild / publishMavenStyle := true
ThisBuild / publishTo in ThisBuild := {
  val nexus = "https://nexus.mnemotix.com/repository"
  if (isSnapshot.value) Some("MNX Nexus" at nexus + "/maven-snapshots/")
  else Some("MNX Nexus" at nexus + "/maven-releases/")
}
ThisBuild / resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.sonatypeRepo("public"),
  Resolver.typesafeRepo("releases"),
  Resolver.typesafeIvyRepo("releases"),
  Resolver.sbtPluginRepo("releases"),
  Resolver.bintrayRepo("owner", "repo"),
  "MNX Nexus (releases)" at "https://nexus.mnemotix.com/repository/maven-releases/",
  "MNX Nexus (snapshots)" at "https://nexus.mnemotix.com/repository/maven-snapshots/"
)
