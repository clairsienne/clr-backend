/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.client

import java.nio.file.{Files, Paths}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.clairsienne.restapi.client.requests.files.PictureRequest
import com.clairsienne.restapi.client.requests.ikos._
import com.clairsienne.restapi.client.requests.sig.SIGReferentielParcelleRequest
import com.clairsienne.restapi.client.requests.sp.{SPActeRequest, SPConventionRequest, SPDCommRequest}
import com.typesafe.scalalogging.LazyLogging
import org.joda.time.DateTime

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 09/03/2020
 */

object RestApiClient extends App with LazyLogging {

  implicit val system = ActorSystem("ClrRestApiClient-" + System.currentTimeMillis())
  implicit val materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  logger.info("Process is starting...")

  val root:String = s"./data/trig-${DateTime.now().toString("yyyyMMddHHmmss")}"
  Files.createDirectories(Paths.get(root))

  val future = Future.sequence(
    Seq(
      new IkosBatimentRequest().process(root),
      new IkosLotRequest().process(root),
      new IkosOperationRequest().process(root),
      new IkosResidenceRequest().process(root),
      new IkosSHARequest().process(root),
      new IkosSURequest().process(root),

      new SIGReferentielParcelleRequest().process(root),
      new PictureRequest().process(root),

      new SPActeRequest().process(root),
      new SPDCommRequest().process(root),
      new SPConventionRequest().process(root)
    )
  )

  Await.result(future, Duration.Inf)
  logger.info("Done.")
  sys exit (0)
}
