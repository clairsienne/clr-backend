/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.client

import java.security.cert.X509Certificate

import akka.http.scaladsl.HttpsConnectionContext
import akka.stream.TLSClientAuth
import com.typesafe.sslconfig.akka.AkkaSSLConfig
import javax.net.ssl._

import scala.collection.immutable

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 11/05/2020
 */

object HTTPSUtils {

  val defaultSslContext: SSLContext = SSLContext.getDefault

  val trustfulSslContext: SSLContext = {
    object NoCheckX509TrustManager extends X509TrustManager {
      override def checkClientTrusted(chain: Array[X509Certificate], authType: String) = ()
      override def checkServerTrusted(chain: Array[X509Certificate], authType: String) = ()
      override def getAcceptedIssuers = Array[X509Certificate]()
    }
    val context = SSLContext.getInstance("TLS")
    context.init(Array[KeyManager](), Array(NoCheckX509TrustManager), null)
    context
  }

  def trustful(
    sslConfig: Option[AkkaSSLConfig] = None,
    enabledCipherSuites: Option[immutable.Seq[String]] = None,
    enabledProtocols: Option[immutable.Seq[String]] = None,
    clientAuth: Option[TLSClientAuth] = None,
    sslParameters: Option[SSLParameters] = None) = https(trustfulSslContext, sslConfig, enabledCipherSuites, enabledProtocols, clientAuth, sslParameters)

  def https(
    sslContext: SSLContext,
    sslConfig: Option[AkkaSSLConfig] = None,
    enabledCipherSuites: Option[immutable.Seq[String]] = None,
    enabledProtocols: Option[immutable.Seq[String]] = None,
    clientAuth: Option[TLSClientAuth] = None,
    sslParameters: Option[SSLParameters] = None) = new HttpsConnectionContext(sslContext, sslConfig, enabledCipherSuites, enabledProtocols, clientAuth, sslParameters)

  def enableSSLVerification(): Unit = HttpsURLConnection.setDefaultSSLSocketFactory(defaultSslContext.getSocketFactory)

  def disableSSLVerification(): Unit = {
    HttpsURLConnection.setDefaultSSLSocketFactory(trustfulSslContext.getSocketFactory)
    val allHostsValid = new HostnameVerifier {
      override def verify(s: String, sslSession: SSLSession): Boolean = true
    }
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid)
  }

}
