/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.client.requests.sig

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import akka.Done
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.clairsienne.models.rdf.RDFSerializer
import com.clairsienne.models.sources.sig.Parcelle
import com.clairsienne.restapi.client.requests.RestApiRequest
import play.api.libs.json.Json

import scala.collection.immutable.Seq
import scala.concurrent.{ExecutionContext, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 09/03/2020
 */

class SIGReferentielParcelleRequest extends RestApiRequest[Parcelle] {
  override val route: String = "/sig/parcelles"
  override val to: String = "parcelles.trig"

  override def process(rootDir:String)(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext): Future[Done] = {
    execute().map { s =>
      val typeCheck = Json.parse(s).validate[Seq[Parcelle]].asOpt
      typeCheck.map { entities =>
        val model = RDFSerializer.getModel().build()
        entities.foreach(b => RDFSerializer.merge(model, Parcelle.toModel(b)))
        Files.write(Paths.get(s"$rootDir/$to"), RDFSerializer.toTriG(model).getBytes(StandardCharsets.UTF_8))
      }
      Done
    }
  }
}
