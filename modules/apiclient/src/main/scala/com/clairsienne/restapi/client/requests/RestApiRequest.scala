/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.client.requests

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.{Authorization, BasicHttpCredentials}
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.clairsienne.restapi.client.{HTTPSUtils, RestApiClientConfiguration}

import scala.collection.immutable.Seq
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 09/03/2020
 */

trait RestApiRequest[T] {
  val route: String
  val to: String

  def request(): HttpRequest = {
    val authorization = Authorization(BasicHttpCredentials(RestApiClientConfiguration.user.get, RestApiClientConfiguration.pass.get))
    HttpRequest(uri = s"${RestApiClientConfiguration.baseUri}$route", headers = Seq(authorization))
  }

  def get()(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) = Http().singleRequest(request(), HTTPSUtils.trustful())

  def executeAsync()(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext): Future[HttpResponse] = Http().singleRequest(request(), HTTPSUtils.trustful())

  def execute()(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) = {
    val resp: HttpResponse = Await.result(executeAsync(), Duration.Inf)
    resp.entity.dataBytes.runFold(ByteString(""))(_ ++ _).map {
      body => body.utf8String
    }
  }

  def process(rootDir:String)(implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext):Future[Done]

}
