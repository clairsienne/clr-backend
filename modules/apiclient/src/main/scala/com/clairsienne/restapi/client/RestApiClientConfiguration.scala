/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.client

import com.typesafe.config.{ Config, ConfigFactory }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 09/03/2020
 */

object RestApiClientConfiguration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("restapi")).getOrElse(ConfigFactory.empty())
  lazy val baseUri: String = conf.getString("uri")
  lazy val user: Option[String] = if (conf.hasPath("user")) Some(conf.getString("user")) else None
  lazy val pass: Option[String] = if (conf.hasPath("pass")) Some(conf.getString("pass")) else None

}
