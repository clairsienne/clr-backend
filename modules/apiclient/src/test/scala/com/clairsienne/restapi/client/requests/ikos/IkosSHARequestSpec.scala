/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.client.requests.ikos

import com.clairsienne.models.sources.ikos.SHA
import com.clairsienne.restapi.client.CLRAkkaTestSpec
import play.api.libs.json.Json

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 11/05/2020
 */

class IkosSHARequestSpec extends CLRAkkaTestSpec {

  override implicit val patienceConfig = PatienceConfig(30.seconds)

  "IkosSHARequest" should {
    val req = new IkosSHARequest
    "request REST API without error" ignore {
      val response = req.get().futureValue
      response.isResponse() shouldBe true
      response.discardEntityBytes()
    }
    "parse API response" ignore {
      val json = req.execute().futureValue
      val typeCheck = Json.parse(json).validate[Seq[SHA]]
      typeCheck.isSuccess shouldBe true
      typeCheck.get.size shouldBe >(0)
      println(s"${typeCheck.get.size} items parsed")
    }
    "process response into RDF" in {
      req.process(rootDir).futureValue
    }
  }

}
