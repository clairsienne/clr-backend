/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Sink }
import com.clairsienne.models.sources.files.Picture
import com.clairsienne.restapi.service.PictureService
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 08/07/2020
 */

class PictureServiceSpec extends WordSpec with BeforeAndAfterAll with Matchers with ScalaFutures {

  override implicit val patienceConfig = PatienceConfig(10.seconds)

  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContext = system.dispatcher

  override protected def afterAll(): Unit = system.terminate()

  "PictureService" should {

    "stream files in a given directory" in {
      val src = PictureService.getPictures()
      val f1 = Flow[Picture].map { entity =>
        println(Json.prettyPrint(Json.toJson(entity)))
      }
      src.via(f1).runWith(Sink.ignore).futureValue
    }
  }
}
