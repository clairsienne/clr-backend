package com.clairsienne.com.restapi

import akka.http.scaladsl.server.{ HttpApp, Route }
import akka.http.scaladsl.settings.ServerSettings
import com.clairsienne.restapi.Routes
import com.typesafe.config.ConfigFactory

object RestApiServer extends HttpApp with App {

  override protected def routes: Route = Routes.api

  val settings = ServerSettings(ConfigFactory.load).withVerboseErrorMessages(true)

  RestApiServer.startServer("0.0.0.0", 8080, settings)

}
