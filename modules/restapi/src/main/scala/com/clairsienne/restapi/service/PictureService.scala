/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.service

import java.io.File

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.clairsienne.models.sources.files.Picture
import com.clairsienne.models.utils.FileUtils
import com.clairsienne.restapi.{ RestApiServerConfiguration, Routes }

import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-09-18
 */

object PictureService {

  def getPictures()(implicit ec: ExecutionContext): Source[Picture, NotUsed] = {
    val files = FileUtils.getFileTree(new File(s"${RestApiServerConfiguration.rootDir.get}/pictures")).filterNot(_.isDirectory).filter(_.exists()).map { f =>
      val filename = f.getName
      val root = filename.split("\\.")(0)
      val tokens = root.split("-")
      Picture(
        filename,
        s"/files/$filename",
        tokens(0),
        tokens(2),
        tokens(1).equals("ext"))
    }
    Source.fromIterator(() => files.toIterator)
  }
}
