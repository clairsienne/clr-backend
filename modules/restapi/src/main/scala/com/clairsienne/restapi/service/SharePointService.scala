/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.service

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.clairsienne.models.sources.sharepoint._
import com.clairsienne.parsers.api.jdbc.CLRJdbcConnectException
import com.clairsienne.parsers.api.jdbc.sqlserver.{ SQLServerConnectionHandler, SQLServerConnectionHandler2 }
import com.clairsienne.parsers.sources.sharepoint.{ SharepointActeAchatQuery, SharepointConventionQuery, SharepointDossierCommercialQuery }

import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-09-18
 */

object SharePointService {

  private var handlerOpt: Option[SQLServerConnectionHandler] = None

  def handler() = {
    if (handlerOpt.isEmpty) handlerOpt = Some(new SQLServerConnectionHandler)
    handlerOpt.get
  }

  def refresh() = handlerOpt = None

  // Les dossiers commerciaux sont stockés dans une autre base de données qui nécessite un handler différent
  private var handlerOpt2: Option[SQLServerConnectionHandler2] = None

  def handler2() = {
    if (handlerOpt2.isEmpty) handlerOpt2 = Some(new SQLServerConnectionHandler2)
    handlerOpt2.get
  }

  def refresh2() = handlerOpt2 = None

  def getActesAchat()(implicit ec: ExecutionContext): Source[ActeAchat, NotUsed] = {
    try {
      val query = new SharepointActeAchatQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getActesAchat()
      }
    }
  }

  def getConventions()(implicit ec: ExecutionContext): Source[Convention, NotUsed] = {
    try {
      val query = new SharepointConventionQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getConventions()
      }
    }
  }

  def getDossiersComm()(implicit ec: ExecutionContext): Source[DossierCommercial, NotUsed] = {
    try {
      val query = new SharepointDossierCommercialQuery()(handler = handler2())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh2()
        getDossiersComm()
      }
    }
  }
}
