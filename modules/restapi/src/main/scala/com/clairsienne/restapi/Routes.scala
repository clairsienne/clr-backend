/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi

import akka.http.scaladsl.common.{ EntityStreamingSupport, JsonEntityStreamingSupport }
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, StatusCodes }
import akka.http.scaladsl.server.Directives.{ complete, concat, get, path, pathSingleSlash, _ }
import akka.http.scaladsl.server.ExceptionHandler
import com.clairsienne.restapi.service.{ IkosService, PictureService, SIGService, SharePointService }
import play.api.libs.json.{ JsString, JsValue, Json }

import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-09-18
 */

object Routes {

  implicit val jsonStreamingSupport: JsonEntityStreamingSupport = EntityStreamingSupport.json()

  val defaultMessage = Json.obj(
    "name" -> JsString("Clairsienne REST API Server"),
    "version" -> JsString("0.1-SNAPSHOT"))

  def defaultEntity = complete(HttpEntity(ContentTypes.`application/json`, Json.stringify(defaultMessage)))

  implicit val prettyPrint: JsValue => String = Json.prettyPrint

  val connectionLostHandler = ExceptionHandler {
    case t: Throwable => complete((StatusCodes.InternalServerError, t.toString))
  }

  val api = get {
    concat(
      pathSingleSlash {
        defaultEntity
      },
      pathPrefix("sp") {
        concat(
          path("actes") {
            complete(SharePointService.getActesAchat())
          },
          path("conventions") {
            complete(SharePointService.getConventions())
          },
          path("dcomm") {
            complete(SharePointService.getDossiersComm())
          })
      },
      pathPrefix("ikos") {
        concat(
          path("batiments") {
            complete(IkosService.getBatiments())
          },
          path("lots") {
            complete(IkosService.getLots())
          },
          path("operations") {
            complete(IkosService.getOperations())
          },
          path("residences") {
            complete(IkosService.getResidences())
          },
          path("sha") {
            complete(IkosService.getSHA())
          },
          path("su") {
            complete(IkosService.getSU())
          })
      },
      pathPrefix("sig") {
        concat(
          path("parcelles") {
            complete(SIGService.getParcelles())
          })
      },
      pathPrefix("pictures") {
        complete(PictureService.getPictures())
      },
      pathPrefix("files") {
        getFromDirectory(s"${RestApiServerConfiguration.rootDir.get}/pictures")
      })
  }
}
