/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.service

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.clairsienne.models.sources.ikos._
import com.clairsienne.parsers.api.jdbc.CLRJdbcConnectException
import com.clairsienne.parsers.api.jdbc.as400.AS400ConnectionHander
import com.clairsienne.parsers.sources.ikos._

import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-09-18
 */

object IkosService {

  private var handlerOpt: Option[AS400ConnectionHander] = None

  def handler() = {
    if (handlerOpt.isEmpty) handlerOpt = Some(new AS400ConnectionHander)
    handlerOpt.get
  }

  def refresh() = handlerOpt = None

  def getBatiments()(implicit ec: ExecutionContext): Source[Batiment, NotUsed] = {
    try {
      val query = new IkosBatimentQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getBatiments()
      }
    }
  }

  def getLots()(implicit ec: ExecutionContext): Source[Lot, NotUsed] = {
    try {
      val query = new IkosLotQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getLots()
      }
    }
  }

  def getOperations()(implicit ec: ExecutionContext): Source[Operation, NotUsed] = {
    try {
      val query = new IkosOperationQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getOperations()
      }
    }
  }

  def getResidences()(implicit ec: ExecutionContext): Source[Residence, NotUsed] = {
    try {
      val query = new IkosResidenceQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getResidences()
      }
    }
  }

  def getSHA()(implicit ec: ExecutionContext): Source[SHA, NotUsed] = {
    try {
      val query = new IkosSHAQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getSHA()
      }
    }
  }

  def getSU()(implicit ec: ExecutionContext): Source[SU, NotUsed] = {
    try {
      val query = new IkosSUQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getSU()
      }
    }
  }

}
