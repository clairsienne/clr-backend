/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.restapi.service

import akka.NotUsed
import akka.stream.scaladsl.Source
import com.clairsienne.models.sources.sig.Parcelle
import com.clairsienne.parsers.api.jdbc.CLRJdbcConnectException
import com.clairsienne.parsers.api.jdbc.postgresql.PostgreSQLConnectionHandler
import com.clairsienne.parsers.sources.sig.SIGReferentielParcelleQuery

import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-09-18
 */

object SIGService {

  private var handlerOpt: Option[PostgreSQLConnectionHandler] = None

  def handler() = {
    if (handlerOpt.isEmpty) handlerOpt = Some(new PostgreSQLConnectionHandler)
    handlerOpt.get
  }

  def refresh() = handlerOpt = None

  def getParcelles()(implicit ec: ExecutionContext): Source[Parcelle, NotUsed] = {
    try {
      val query = new SIGReferentielParcelleQuery()(handler = handler())
      query.toSource()
    } catch {
      case _: CLRJdbcConnectException => {
        refresh()
        getParcelles()
      }
    }
  }

}
