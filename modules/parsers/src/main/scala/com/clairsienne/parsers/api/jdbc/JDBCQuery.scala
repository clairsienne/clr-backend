/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc

import java.sql._

import akka.stream.scaladsl.Source
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ ExecutionContext, Future }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

trait JDBCQuery[T] extends LazyLogging {

  val handler: JDBCConnectionHandler

  val queryString: String

  def mapRow(resultSet: ResultSet): T

  def rowToString(resultSet: ResultSet): String

  var conn: Connection = _
  var stmt: Statement = _
  var resultSet: ResultSet = _

  def mapString(resultSet: ResultSet): Iterator[String] = {
    new JDBCResultSetIterator[String](resultSet) {
      override def next(): String = rowToString(resultSet)
    }
  }

  def executeRaw(autocommit: Boolean = true)(implicit ec: ExecutionContext) = {
    try {
      conn = handler.connect()
      if (!autocommit) conn.setAutoCommit(false) // Commit changes manually
      stmt = conn.createStatement()
      resultSet = stmt.executeQuery(queryString)
      if (!autocommit) conn.commit() // Connection must be on a unit-of-work boundary to allow close
      resultSet
    } catch {
      case cnfe: ClassNotFoundException => throw new CLRJdbcDriverException("Unable to find the driver", cnfe)
      case sqlte: SQLTimeoutException => throw new CLRJdbcConnectException("Unable to connect the server", sqlte)
      case sqle: SQLException => throw new CLRJdbcSQLException("Unable to execute the query", sqle)
    }
  }

  def execute(autocommit: Boolean = true)(implicit ec: ExecutionContext): Iterator[T] = {
    try {
      conn = handler.connect()
      if (!autocommit) conn.setAutoCommit(false) // Commit changes manually
      stmt = conn.createStatement()
      resultSet = stmt.executeQuery(queryString)
      val iterator = Iterator.from(0).takeWhile(_ => resultSet.next()).map(_ => mapRow(resultSet))
      if (!autocommit) conn.commit() // Connection must be on a unit-of-work boundary to allow close
      iterator
    } catch {
      case cnfe: ClassNotFoundException => throw new CLRJdbcDriverException("Unable to find the driver", cnfe)
      case sqlte: SQLTimeoutException => throw new CLRJdbcConnectException("Unable to connect the server", sqlte)
      case sqle: SQLException => throw new CLRJdbcSQLException("Unable to execute the query", sqle)
    }
  }

  def toRDF(autocommit: Boolean = true)(implicit ec: ExecutionContext): Iterator[String] = {
    try {
      conn = handler.connect()
      if (!autocommit) conn.setAutoCommit(false) // Commit changes manually
      stmt = conn.createStatement()
      resultSet = stmt.executeQuery(queryString)
      val iterator = Iterator.from(0).takeWhile(_ => resultSet.next()).map(_ => rowToString(resultSet))
      if (!autocommit) conn.commit() // Connection must be on a unit-of-work boundary to allow close
      iterator
    } catch {
      case cnfe: ClassNotFoundException => throw new CLRJdbcDriverException("Unable to find the driver", cnfe)
      case sqlte: SQLTimeoutException => throw new CLRJdbcConnectException("Unable to connect the server", sqlte)
      case sqle: SQLException => throw new CLRJdbcSQLException("Unable to execute the query", sqle)
    }
  }

  def cleanup() = {
    resultSet.close() // Close the ResultSet
    stmt.close() // Close the Statement
  }

  def executeAsync()(implicit ec: ExecutionContext): Future[Iterator[T]] = Future {
    execute()
  }

  def toSource()(implicit ec: ExecutionContext) = Source.fromIterator { () => execute() }

  def toRDFSource()(implicit ec: ExecutionContext) = Source.fromIterator { () => toRDF() }
}
