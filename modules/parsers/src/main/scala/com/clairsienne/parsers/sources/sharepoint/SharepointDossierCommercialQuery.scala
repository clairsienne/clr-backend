/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.sharepoint

import java.sql.ResultSet

import com.clairsienne.models.sources.sharepoint.DossierCommercial
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.sqlserver.SQLServerConnectionHandler2

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

class SharepointDossierCommercialQuery()(implicit override val handler: SQLServerConnectionHandler2) extends JDBCQuery[DossierCommercial] {

  override val queryString: String =
    """
      |SELECT  Id,
      |CONCAT( DirName,'/', [LeafName]) as URLFichierSource,
      |TimeCreated,
      |TimeLastModified,
      |[AllUserData].tp_ColumnSet.value('nvarchar13[1]', 'nvarchar(max)') as identifiantresidence,
      |	case B.tp_ListId when '4A9BE38D-ED72-42E4-A981-A10911BA90F6' then B.tp_ColumnSet.value('nvarchar1[1]', 'nvarchar(max)') else '' end as codepostal
      |FROM [WSS_Content_DPGI].[dbo].[AllDocs] inner join [WSS_Content_DPGI].[dbo].[AllUserData]
      |ON [WSS_Content_DPGI].[dbo].[AllUserData].tp_ListId=ListId
      |and [WSS_Content_DPGI].[dbo].[AllUserData].tp_DocId=Id
      |inner join	AllUserData	B
      |on B.tp_id=AllUserData.tp_ColumnSet.value('int1[1]','int')
      |where  WebId ='7F0DDFF3-EE8B-42E7-B7BA-CC0D71911CC8' and ListId='FEC462A8-39AA-49F3-AB4E-D95E646178D3'
      |and B.tp_ListId=  '4A9BE38D-ED72-42E4-A981-A10911BA90F6'
      |and DeleteTransactionId=0 and dirty=0
      |and ExtensionForFile='pdf'
    """.stripMargin

  override def mapRow(resultSet: ResultSet): DossierCommercial = DossierCommercial(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
