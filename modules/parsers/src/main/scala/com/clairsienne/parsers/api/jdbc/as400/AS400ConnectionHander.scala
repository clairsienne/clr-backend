/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc.as400

import com.clairsienne.parsers.api.jdbc.JDBCConnectionHandler

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class AS400ConnectionHander extends JDBCConnectionHandler {

  override val driverClassName: String = "com.ibm.as400.access.AS400JDBCDriver"

  lazy val host = AS400Configuration.host.getOrElse(throw new IllegalArgumentException("AS400 host not found in provided configuration."))
  lazy val port = AS400Configuration.port.getOrElse(throw new IllegalArgumentException("AS400 port not found in provided configuration."))
  lazy val user = AS400Configuration.user.getOrElse(throw new IllegalArgumentException("AS400 user not found in provided configuration."))
  lazy val password = AS400Configuration.pass.getOrElse(throw new IllegalArgumentException("AS400 password not found in provided configuration."))

  lazy val url = s"jdbc:as400://$host:$port"

}
