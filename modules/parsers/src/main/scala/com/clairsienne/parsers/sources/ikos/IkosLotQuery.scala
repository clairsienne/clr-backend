/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.sources.ikos.Lot
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.as400.AS400ConnectionHander

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class IkosLotQuery()(implicit override val handler: AS400ConnectionHander) extends JDBCQuery[Lot] {

  override val queryString: String =
    """
      |SELECT DISTINCT
      |IKGLFIC_CL.UGP.GGCDOGA as identifiantorganisme,
      |IKGLFIC_CL.UGP.GGNOUG as identifiantlot,
      |IKGLFIC_CL.UGP.GGCDPAT2 as identifiantresidence,
      |IKGLFIC_CL.UGP.GGCDPAT3 as identifiantbatiment,
      |IKGLFIC_CL.UGP.GGCDPAT4 as identifiantentree,
      |IKGLFIC_CL.TETAGEP.J7NIVETAG as niveaubatiment,
      |GGNOPORTE as numeroPorte,
      |IKGLFIC_CL.TINDCOP.K9LBLINDCO as typehabitation,
      |IKGLFIC_CL.TTYPUGP.K3LBRTYPUG as typologielot,
      |IKGLFIC_CL.TCATUGP.J1LBLCATUG as modefinancement,
      |IKGLFIC_CL.TNATUGP.JILBLNATUG as naturelot,
      |MPADRPORUE as adresse,
      |MPADRPOSUI as adressesuite,
      |MPADRPOLOC as commune,BVLBLEPCI as epci,MPCDTDEPT as codedepartement,trim(MPCDTDEPT)||trim(MPNOINSEEC) as codeinsee,MPCDPOSTAL as codepostal,
      |IKGLFIC_CL.UGP.GGINDLIBR2 as lotenfindegestion,
      | case (GGCDELIBR1) when
      |'IAA' then 'Sous-traite' else 'Direct' end as modeGestion
      |FROM
      |IKGLFIC_CL.UGP
      |INNER JOIN
      |IKGLFIC_CL.PATP
      |ON
      |(
      |IKGLFIC_CL.UGP.GGCDPAT1 = IKGLFIC_CL.PATP.HPCDPAT1)
      |AND (
      |IKGLFIC_CL.UGP.GGCDPAT2 = IKGLFIC_CL.PATP.HPCDPAT2)
      |AND (
      |IKGLFIC_CL.UGP.GGCDPAT3 = IKGLFIC_CL.PATP.HPCDPAT3)
      |AND (
      |IKGLFIC_CL.UGP.GGCDPAT4 = IKGLFIC_CL.PATP.HPCDPAT4)
      |INNER JOIN ikglfic_cl.adrposp
      |ON HPNADRPOS=MPNADRPOS
      |inner join IKGLFIC_CL.TINCMCP
      |ON
      |TKCDTDEPT=HPCDTDEPT
      |AND
      |TKNOINSEEC=HPNOINSEEC
      |INNER JOIN IKGLFIC_CL.TBEPCIP
      |ON
      |TKCDEPCI=BVCDEPCI
      |INNER JOIN IKGLFIC_CL.TINDCOP
      |ON
      |IKGLFIC_CL.UGP.GGCDTINDCO = IKGLFIC_CL.TINDCOP.K9CDTINDCO
      |INNER JOIN IKGLFIC_CL.TTYPUGP
      |ON
      |IKGLFIC_CL.UGP.GGCDTTYPUG = IKGLFIC_CL.TTYPUGP.K3CDTTYPUG
      |INNER JOIN IKGLFIC_CL.TCATUGP
      |ON
      |IKGLFIC_CL.UGP.GGCDTCATUG = IKGLFIC_CL.TCATUGP.J1CDTCATUG
      |INNER JOIN IKGLFIC_CL.TNATUGP
      |ON
      |IKGLFIC_CL.UGP.GGCDTNATUG = IKGLFIC_CL.TNATUGP.JICDTNATUG
      |
      |LEFT JOIN ikglfic_cl.TETAGEP
      |ON
      |IKGLFIC_CL.UGP.GGCDTETAGE= ikglfic_cl.TETAGEP.J7CDTETAGE
      |AND GGCDTNATUG  NOT IN ('LVS')
      |AND  GGCDOGA='CLA' AND GGINDANNUL<>'O'
      |""".stripMargin

  override def mapRow(resultSet: ResultSet): Lot = Lot(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
