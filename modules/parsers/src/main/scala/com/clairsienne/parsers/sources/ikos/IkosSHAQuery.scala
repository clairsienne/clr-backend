/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.sources.ikos.SHA
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.as400.AS400ConnectionHander

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class IkosSHAQuery()(implicit override val handler: AS400ConnectionHander) extends JDBCQuery[SHA] {

  override val queryString: String =
    """
      |SELECT t1.gtnoug AS identifiantlot,T1.GTQTEFACT AS SHA FROM IKGLFIC_CL.UGHTANP T1 INNER JOIN
      |(
      |	SELECT t2.GTNOUG ,MAX(GTDTDBVL) AS date_max
      |	FROM ikglfic_cl.UGHTANP t2
      |	where t2.GTCDTTANTM in ('SHA')
      |	GROUP BY t2.GTNOUG
      |) tmp
      |	ON tmp.GTNOUG = t1.GTNOUG
      |	AND tmp.date_max = t1.GTDTDBVL
      |	and t1.GTCDTTANTM in ('SHA')
      |
    """.stripMargin

  override def mapRow(resultSet: ResultSet): SHA = SHA(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
