/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc

import java.sql.{ Connection, DriverManager }

import com.typesafe.scalalogging.LazyLogging

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

trait JDBCConnectionHandler extends LazyLogging {

  val driverClassName: String
  val url: String
  val user: String
  val password: String

  def connect(): Connection = {
    try {
      Class.forName(driverClassName)
    } catch {
      case e: ClassNotFoundException =>
        logger.error("Driver not found", Some(e))
        sys.exit()
    }

    try {
      val conn = DriverManager.getConnection(url, user, password)
      conn
    } catch {
      case t: Throwable =>
        logger.error("Unable to connect to the server", Some(t))
        throw new CLRJdbcConnectException("Unable to connect to the server", t)
    }
  }
}
