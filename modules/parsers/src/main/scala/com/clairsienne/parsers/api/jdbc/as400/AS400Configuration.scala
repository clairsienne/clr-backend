/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc.as400

import com.typesafe.config.{ Config, ConfigFactory }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

object AS400Configuration {
  lazy val conf: Config = Option(ConfigFactory.load().getConfig("as400")).getOrElse(ConfigFactory.empty())
  lazy val host: Option[String] = if (conf.hasPath("host")) Some(conf.getString("host")) else None
  lazy val port: Option[Int] = if (conf.hasPath("port")) Some(conf.getInt("port")) else None
  lazy val user: Option[String] = if (conf.hasPath("user")) Some(conf.getString("user")) else None
  lazy val pass: Option[String] = if (conf.hasPath("pass")) Some(conf.getString("pass")) else None
  lazy val dbname: Option[String] = if (conf.hasPath("dbname")) Some(conf.getString("dbname")) else None
}
