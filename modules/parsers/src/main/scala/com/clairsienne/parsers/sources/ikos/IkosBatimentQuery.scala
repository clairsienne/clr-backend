/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.sources.ikos.Batiment
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.as400.AS400ConnectionHander

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class IkosBatimentQuery()(implicit override val handler: AS400ConnectionHander) extends JDBCQuery[Batiment] {

  override val queryString: String =
    """
    |SELECT DISTINCT
    |IKGLFIC_CL.UGP.GGCDOGA as identifiantorganisme,
    |IKGLFIC_CL.UGP.GGCDOGA || IKGLFIC_CL.UGP.GGCDPAT2 ||IKGLFIC_CL.UGP.GGCDPAT3||IKGLFIC_CL.UGP.GGCDPAT4 as idbatiment,
    |IKGLFIC_CL.UGP.GGCDPAT2 as identifiantresidence,
    |IKGLFIC_CL.UGP.GGCDPAT3 as identifiantbatiment,
    |IKGLFIC_CL.UGP.GGCDPAT4 as identifiantentree,
    |MPADRPORUE as adresseresidence,
    |MPADRPOSUI as adressesuite,
    |MPADRPOLOC as commune,BVLBLEPCI as epci,MPCDTDEPT as codedepartement,trim(MPCDTDEPT)||trim(MPNOINSEEC) as codeinsee,MPCDPOSTAL as codepostal,
    |
    |case GGCDTINDCO when 'CLT' then 'Collectif' when 'IDV' then 'Individuel'  end as typehabitation
    |FROM
    |IKGLFIC_CL.UGP
    |INNER JOIN
    |IKGLFIC_CL.PATP
    |ON
    |(
    |IKGLFIC_CL.UGP.GGCDPAT1 = IKGLFIC_CL.PATP.HPCDPAT1)
    |AND (
    |IKGLFIC_CL.UGP.GGCDPAT2 = IKGLFIC_CL.PATP.HPCDPAT2)
    |AND (
    |IKGLFIC_CL.UGP.GGCDPAT3 = IKGLFIC_CL.PATP.HPCDPAT3)
    |AND (
    |IKGLFIC_CL.UGP.GGCDPAT4 = IKGLFIC_CL.PATP.HPCDPAT4)
    |LEFT JOIN ikglfic_cl.TETAGEP
    |ON
    |IKGLFIC_CL.UGP.GGCDTETAGE= ikglfic_cl.TETAGEP.J7CDTETAGE
    |INNER JOIN ikglfic_cl.adrposp
    |ON HPNADRPOS=MPNADRPOS
    |inner join IKGLFIC_CL.TINCMCP
    |ON
    |TKCDTDEPT=HPCDTDEPT
    |AND
    |TKNOINSEEC=HPNOINSEEC
    |INNER JOIN IKGLFIC_CL.TBEPCIP
    |ON
    |TKCDEPCI=BVCDEPCI
    |WHERE HPCDOGA='CLA' AND GGCDOGA='CLA' AND  GGINDANNUL<>'O' AND GGCDTNATUG  NOT IN ('LVS','LPD','LRF','LAC')
    |AND GGCDPAT4<>''
    |AND GGCDELIBR1 NOT IN ('DML','VDU','VDO','VDF','RGP','NSP')
    |AND GGCDELIBR2 <>'O'  AND HPCDPAT4<>''
    |""".stripMargin

  //  override val queryString: String =
  //    """
  //      |SELECT DISTINCT
  //      |IKGLFIC_CL.UGP.GGCDOGA as identifiantorganisme,
  //      |IKGLFIC_CL.UGP.GGCDOGA || IKGLFIC_CL.UGP.GGCDPAT2 ||IKGLFIC_CL.UGP.GGCDPAT3||IKGLFIC_CL.UGP.GGCDPAT4 as idbatiment,
  //      |IKGLFIC_CL.UGP.GGCDPAT2 as identifiantresidence,
  //      |IKGLFIC_CL.UGP.GGCDPAT3 as identifiantbatiment,
  //      |IKGLFIC_CL.UGP.GGCDPAT4 as identifiantentree,
  //      |MPADRPORUE as adresseresidence,
  //      |MPADRPOSUI as adressesuite,
  //      |MPADRPOLOC as commune,
  //      |BVLBLEPCI as epci,
  //      |MPCDTDEPT as codedepartement,
  //      |trim(MPCDTDEPT)||trim(MPNOINSEEC) as codeinsee,
  //      |MPCDPOSTAL as codepostal,
  //      |case GGCDTINDCO when 'CLT' then 'Collectif' when 'IDV' then 'Individuel'  end as typehabitation
  //      |FROM
  //      |IKGLFIC_CL.UGP
  //      |INNER JOIN
  //      |IKGLFIC_CL.PATP
  //      |ON
  //      |(
  //      |IKGLFIC_CL.UGP.GGCDPAT1 = IKGLFIC_CL.PATP.HPCDPAT1)
  //      |AND (
  //      |IKGLFIC_CL.UGP.GGCDPAT2 = IKGLFIC_CL.PATP.HPCDPAT2)
  //      |AND (
  //      |IKGLFIC_CL.UGP.GGCDPAT3 = IKGLFIC_CL.PATP.HPCDPAT3)
  //      |AND (
  //      |IKGLFIC_CL.UGP.GGCDPAT4 = IKGLFIC_CL.PATP.HPCDPAT4)
  //      |LEFT JOIN ikglfic_cl.TETAGEP
  //      |ON
  //      |IKGLFIC_CL.UGP.GGCDTETAGE= ikglfic_cl.TETAGEP.J7CDTETAGE
  //      |INNER JOIN ikglfic_cl.adrposp
  //      |ON HPNADRPOS=MPNADRPOS
  //      |inner join IKGLFIC_CL.TINCMCP
  //      |ON
  //      |TKCDTDEPT=HPCDTDEPT
  //      |AND
  //      |TKNOINSEEC=HPNOINSEEC
  //      |INNER JOIN IKGLFIC_CL.TBEPCIP
  //      |ON
  //      |TKCDEPCI=BVCDEPCI
  //      |WHERE HPCDOGA='CLA' AND GGCDOGA='CLA' AND  GGINDANNUL<>'O' AND GGCDTNATUG  NOT IN ('LVS','LPD','LRF','LAC')
  //      |AND GGCDPAT4<>''
  //      |AND GGCDELIBR1 NOT IN ('DML','VDU','VDO','VDF','RGP','NSP')
  //      |AND GGCDELIBR2 <>'O'  AND HPCDPAT4<>''
  //      |
  //    """.stripMargin

  override def mapRow(resultSet: ResultSet): Batiment = Batiment(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
