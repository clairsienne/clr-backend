/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.sources.ikos.Operation
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.as400.AS400ConnectionHander

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class IkosOperationQuery()(implicit override val handler: AS400ConnectionHander) extends JDBCQuery[Operation] {

  override val queryString: String =
    """
      |SELECT
      |    OMCDOGA as identifiantorganisme,
      |    OMCDPROGRA||OTCDOPEPRO||ONCDTRANCH as idoperation,
      |    OMCDPROGRA as identifiantoperation,
      |    OTCDOPEPRO as codeoperation,
      |    ONCDTRANCH as numerotranche,
      |    TRIM(OTLIBOPPGM)||' '||TRIM(OTLBLSUITE) as nomoperation,
      |    SXLBLTYOPE as natureoperation,
      |    OMCDPAT2A as identifiantresidence,
      |    trim(OTCDTDEPT)||trim(OTNOINSEEC) as codeinsee,
      |    OTSURFHAB as SHA,
      |    OTSURFUTL as SU,
      |    OTADRRUE as adresse,
      |    OTADRSUITE as adressesuite,
      |    OTCDPOSTAL as codepostal,
      |    OTLOCALITE as commune
      |FROM
      |    IKGFFIC_CL.KPROGRP INNER JOIN
      |    IKGFFIC_CL.KPGROPP ON IKGFFIC_CL.KPGROPP.OTCDPROGRA =IKGFFIC_CL.KPROGRP.OMCDPROGRA
      |    INNER JOIN
      |IKGFFIC_CL.KPGOPTP ON IKGFFIC_CL.KPGOPTP.ONCDPROGRA=IKGFFIC_CL.KPGROPP.OTCDPROGRA
      |AND IKGFFIC_CL.KPGROPP.OTCDOPEPRO=IKGFFIC_CL.KPGOPTP.ONCDOPEPRO
      |INNER JOIN
      |IKGFFIC_CL.KTYOPEP
      |ON
      |IKGFFIC_CL.KTYOPEP.SXCDTYPOPE=IKGFFIC_CL.KPGROPP.OTCDTYPOPE
      |where OMCDOGA='CLA' and OTCDOGA='CLA' and (OTLIBOPPGM not like '%NE PAS%' or OTLIBOPPGM not like 'ABANDON%' or OTLBLSUITE like '%NE PAS%' )
      |
      |
      |
      |""".stripMargin

  override def mapRow(resultSet: ResultSet): Operation = Operation(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
