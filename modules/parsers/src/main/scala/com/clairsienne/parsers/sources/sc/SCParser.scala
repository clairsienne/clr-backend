/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.sc

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import com.clairsienne.models.sources.sc.SCRow
import com.clairsienne.parsers.api.xlsx.XLSXParser
import org.apache.poi.ss.usermodel.Row

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

class SCParser(override val filename: String)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends XLSXParser[SCRow](true) {

  override def convert(): Flow[Row, SCRow, NotUsed] = Flow[Row].map { row =>
    val iseq = row.cellIterator().asScala.toVector
    SCRow(
      annee = getCellValue(iseq(0)).toInt,
      etiquette = getCellValue(iseq(1)),
      notSatisfied = getCellValue(iseq(2)).toInt,
      satisfied = getCellValue(iseq(3)).toInt,
      total = getCellValue(iseq(4)).toInt)
  }

  override def publish(): Flow[SCRow, SCRow, NotUsed] = Flow[SCRow].map { row =>
    println(row)
    row
  }

}
