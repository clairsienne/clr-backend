/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.sharepoint

import java.sql.ResultSet

import com.clairsienne.models.sources.sharepoint.Convention
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.sqlserver.SQLServerConnectionHandler

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

class SharepointConventionQuery()(implicit override val handler: SQLServerConnectionHandler) extends JDBCQuery[Convention] {
  override val queryString: String =
    """select distinct id,
      |concat(dirname,'/', LeafName) as URLFichierSource,
      |AllUserData.tp_ColumnSet.value('nvarchar12[1]', 'nvarchar(max)') as identifiantresidence,
      |AllUserData.tp_ColumnSet.value('nvarchar3[1]', 'nvarchar(max)') as typeDossier,
      |AllUserData.tp_ColumnSet.value('nvarchar4[1]', 'nvarchar(max)') as numAvenant,
      |AllUserData.tp_ColumnSet.value('nvarchar5[1]', 'nvarchar(max)') as numConvention,
      |AllUserData.tp_ColumnSet.value('datetime4[1]','datetime') as dateSignature,
      |AllUserData.tp_ColumnSet.value('datetime5[1]','datetime') as dateExpiration,
      |case B.tp_ListId when 'C1E59C6B-250F-4145-B748-A1635524B11E' then B.tp_ColumnSet.value('nvarchar3[1]', 'nvarchar(max)') else '' end as codepostal
      |from AllDocs
      |inner join AllUserData on tp_id=cast(replace(right(DirName,4),'/','') as integer) and ListId=tp_ListId
      |inner join AllUserData	B on B.tp_id=AllUserData.tp_ColumnSet.value('int2[1]','int')
      |where ListId ='16051866-4CC7-4BDD-ABF8-4A38D17CA89F'
      |and B.tp_ListId=  'C1E59C6B-250F-4145-B748-A1635524B11E'
      |and HasStream=1			 and  AllUserData.tp_ColumnSet.value('nvarchar6[1]', 'nvarchar(max)')='Complet'
      |""".stripMargin

  override def mapRow(resultSet: ResultSet): Convention = Convention(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
