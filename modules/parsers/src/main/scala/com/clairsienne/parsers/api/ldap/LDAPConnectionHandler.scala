/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.ldap

import com.typesafe.scalalogging.LazyLogging

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 27/01/2020
 */

class LDAPConnectionHandler extends LazyLogging {

  //  val dnResolver = new SearchDnResolver(new DefaultConnectionFactory(connConfig))
  //  val simpleLdap = new Ldap()
  //  val ldapWithBlockingPool = new Ldap(new Settings(ConfigFactory.parseString(s"""
  //  ldap {
  //    host = "ldap://localhost:8888"
  //    base-dn = "DC=logis,DC=local"
  //    bind-dn = "usermnemotix"
  //    bind-password = "Mnemotix@33%"
  //    connection-timeout = 10 seconds
  //    response-timeout = 10 seconds
  //    pool {
  //      enable-pool = true
  //      min-pool-size = 1
  //      max-pool-size = 1
  //    }
  //  }
  //  """)))

}
