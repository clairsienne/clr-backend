/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.sharepoint

import java.sql.ResultSet

import com.clairsienne.models.sources.sharepoint.ActeAchat
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.sqlserver.SQLServerConnectionHandler

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

class SharepointActeAchatQuery()(implicit override val handler: SQLServerConnectionHandler) extends JDBCQuery[ActeAchat] {
  override val queryString: String =
    """SELECT  [Id],
      |CONCAT( DirName,'/', [LeafName]) as URLFichierSource
      |,[TimeCreated]
      |,[TimeLastModified]
      |, AllUserData.tp_ColumnSet.value('nvarchar11[1]', 'nvarchar(max)') as identifiantresidence,
      |AllUserData.tp_ColumnSet.value('nvarchar16[1]', 'nvarchar(max)') as identifiantoperation,
      |AllUserData.tp_ColumnSet.value('nvarchar12[1]', 'nvarchar(max)') as nomresidence,
      |AllUserData.tp_ColumnSet.value('nvarchar10[1]', 'nvarchar(max)') as typeActe,
      |AllUserData.tp_ColumnSet.value('nvarchar14[1]', 'nvarchar(max)') as tiersvendeur,
      |AllUserData.tp_ColumnSet.value('datetime1[1]','datetime') as dateSignature,
      |AllUserData.tp_ColumnSet.value('nvarchar13[1]','nvarchar(max)') as commune
      |FROM AllDocs inner join AllUserData
      |ON AllUserData.tp_ListId=ListId
      |and AllUserData.tp_DocId=Id
      |where  WebId ='24ABAFD3-C67C-4613-A027-AC5B2F5CFDC4' and ListId='C12D2ECA-66BC-4D1A-8017-37D7AC38FA4D'
      |and  AllUserData.tp_ColumnSet.value('nvarchar10[1]', 'nvarchar(max)')='ACHAT'
      |and AllUserData.tp_ColumnSet.value('nvarchar15[1]', 'nvarchar(max)') IN ('ACTE','ACTE COMPLEMENTAIRE','ACTE RECTIFICATIF','BAIL EMPHYTHEOTIQUE','COMPLEMENT DE PARCELLES')
      |order by AllUserData.tp_ColumnSet.value('nvarchar11[1]', 'nvarchar(max)')
      |""".stripMargin

  override def mapRow(resultSet: ResultSet): ActeAchat = ActeAchat(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
