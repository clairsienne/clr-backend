/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.sources.ikos.Residence
import com.clairsienne.parsers.api.jdbc.JDBCQuery
import com.clairsienne.parsers.api.jdbc.as400.AS400ConnectionHander

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class IkosResidenceQuery()(implicit override val handler: AS400ConnectionHander) extends JDBCQuery[Residence] {

  override val queryString: String =
    """
      |select HPCDOGA as identifiantorganisme,
      |HPCDPAT2 as identifiantresidence,
      |MPLIBPATAD as nomresidence,
      |case(MIN(GGCDTINDCO)||MAX(GGCDTINDCO)) when 'CLTCLT' then 'Collectif' when 'IDVIDV' then 'Individuel' else 'Mixte' end as typehabitation,
      |MIN(cast(date(Substr(digits(HPDTEACQUI), 1, 4)||'-'||Substr(digits(HPDTEACQUI), 5, 2)||'-'||Substr(digits(HPDTEACQUI), 7, 2)) as timestamp)) as dateacquisition,
      |MIN(cast(date(Substr(digits(HPDTECSTR), 1, 4)||'-'||Substr(digits(HPDTECSTR), 5, 2)||'-'||Substr(digits(HPDTECSTR), 7, 2)) as timestamp)) as dateconstruction,
      |MIN(cast(date(Substr(digits(GGDTMSUG), 1, 4)||'-'||Substr(digits(GGDTMSUG), 5, 2)||'-'||Substr(digits(GGDTMSUG), 7, 2)) as timestamp)) as datemiseensercice,
      |MIN(cast(date(Substr(digits(GGDTLIVRAI), 1, 4)||'-'||Substr(digits(GGDTLIVRAI), 5, 2)||'-'||Substr(digits(GGDTLIVRAI), 7, 2)) as timestamp)) as dateLivraison,
      |MIN(cast(date(Substr(digits(GGDTERCTRV), 1, 4)||'-'||Substr(digits(GGDTERCTRV), 5, 2)||'-'||Substr(digits(GGDTERCTRV), 7, 2)) as timestamp))  as datereceptiontravaux,
      |GDLBLMOACQ as modeAcquisition,
      |case MIN(GGCDELIBR1) when 'IAA' then 'Sous-traite' else 'Direct' end as modeGestion,
      |case (HPCDORG2A) when '' then 'non affecte' when 'AGU' then 'Antenne Guyenne' when 'AOC' then 'Antenne Oceane' when 'AMA' then 'Antenne Mascaret' when 'AFDG' then 'Antenne Foyer' end as antenne,
      |MPADRPORUE as adresse,
      |MPADRPOSUI as adressesuite,
      |MPADRPOLOC as commune,
      |BVLBLEPCI as epci,
      |MPCDTDEPT as codedepartement,
      |trim(MPCDTDEPT)||trim(MPNOINSEEC) as codeinsee,
      |MPCDPOSTAL as codepostal,
      |cast((replace(MPLATIT,',','.'))as float) as latitude,
      |cast((replace(mplongit,',','.'))as float) as longitude,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2 and GGCDTNATUG NOT IN ('GRG','PAK','CEL','PMO','LVS','LPD','LRF','LAC') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date )  as nblogementinitial,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2 and GGCDTNATUG NOT IN ('GRG','PAK','CEL','PMO','LVS','LPD','LRF','LAC') AND GGCDELIBR1 NOT IN ('DML','VDU','VDO','VDF','RGP','NSP') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date )  as nblogementgere,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2 and GGCDTNATUG NOT IN ('GRG','PAK','CEL','PMO','LVS','LPD','LRF','LAC') AND GGCDELIBR1 NOT IN ('DML','VDU','VDO','VDF','RGP','NSP') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date and  GGCDTINDCO='CLT')  as nblogementcollectif,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2 and GGCDTNATUG NOT IN ('GRG','PAK','CEL','PMO','LVS','LPD','LRF','LAC') AND GGCDELIBR1 NOT IN ('DML','VDU','VDO','VDF','RGP','NSP') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date and  GGCDTINDCO='IDV' )  as nblogementindividuel,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2 and ggcdtnatug in ('GRG','PAK','PMO','CEL','TNU') AND GGCDELIBR1 NOT IN ('DML','VDU','VDO','VDF','RGP','NSP') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date) as nbannexe,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2  AND GGCDELIBR1  IN ('DML','NSP','RGP') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date) as nbdemolis,
      |(SELECT COUNT(*) FROM ikglfic_cl.ugp where IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2 and  GGCDELIBR1  IN ('VDU','VDO','VDF') AND cast(date(Substr(digits((GGDTMSUG)), 1, 4)||'-'||Substr(digits((GGDTMSUG)), 5, 2)||'-'||Substr(digits((GGDTMSUG)), 7, 2)) as timestamp)<= current date) as nbvendus
      |from  IKGLFIC_CL.PATP
      |INNER JOIN ikglfic_cl.adrposp
      |ON HPNADRPOS=MPNADRPOS
      |inner join IKGLFIC_CL.TINCMCP
      |ON
      |TKCDTDEPT=HPCDTDEPT
      |AND
      |TKNOINSEEC=HPNOINSEEC
      |INNER JOIN IKGLFIC_CL.TBEPCIP
      |ON
      |TKCDEPCI=BVCDEPCI
      |INNER JOIN IKGLFIC_CL.TMDACQP
      |ON IKGLFIC_CL.TMDACQP.GDINDACQUI= ikglfic_cl.patp.HPINDACQUI
      |INNER JOIN ikglfic_cl.UGP
      |ON
      |IKGLFIC_CL.UGP.GGCDPAT2= ikglfic_cl.patp.hpcdpat2
      |where HPCDOGA='CLA' AND GGCDOGA='CLA' AND HPINDANNUL<>'O' AND HPCDPAT2<>'' and HPCDPAT3='' and GGCDTCATUG NOT IN 'PSA'
      |AND HPDTFNVL=0 AND HPCDPAT1<>'ACCE'
      |group by HPCDOGA,HPCDPAT2,MPLIBPATAD,GDLBLMOACQ,HPCDORG2A,MPADRPORUE,MPADRPOLOC,MPCDTDEPT,MPNOINSEEC,MPNOINSEEC,MPLATIT,mplongit,BVLBLEPCI,MPCDPOSTAL,MPADRPOSUI
    """.stripMargin

  override def mapRow(resultSet: ResultSet): Residence = Residence(resultSet)

  override def rowToString(resultSet: ResultSet): String = ???
}
