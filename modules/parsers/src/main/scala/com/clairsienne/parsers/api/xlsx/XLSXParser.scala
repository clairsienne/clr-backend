/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.xlsx

import java.io.File

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Sink, Source }
import akka.{ Done, NotUsed }
import com.clairsienne.parsers.api.GenericParser
import org.apache.poi.ss.usermodel._

//import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.{ Failure, Success }

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

abstract class XLSXParser[T](val skipFirstLine: Boolean = false)(implicit override val system: ActorSystem, override val materializer: ActorMaterializer, override val ec: ExecutionContext) extends GenericParser {

  val filename: String
  var wb: Workbook = _
  lazy val formatter = new DataFormatter()

  def init() = {
    wb = WorkbookFactory.create(new File(filename))
  }

  def close() = {
    wb.close()
  }

  def convert(): Flow[Row, T, NotUsed]

  def publish(): Flow[T, T, NotUsed]

  override def process(): Future[Done] = {
    init()
    val iter = wb.getSheetAt(0).rowIterator().asScala
    if (skipFirstLine) iter.next()
    val src: Source[Row, NotUsed] = Source.fromIterator(() => iter)
    val f = src.via(convert()).via(publish()).runWith(Sink.ignore)
    f.onComplete {
      case Success(_) => {
        logger.info("Parsing done.")
        close()
      }
      case Failure(error) => {
        logger.error("Parsing failed.", error)
      }
    }
    f
  }

  def getCellAddress(cell: Cell) = (cell.getAddress.getRow, cell.getAddress.getColumn)

  def getCellValue(cell: Cell) = formatter.formatCellValue(cell)
}
