/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc.sqlserver

import com.clairsienne.parsers.api.jdbc.JDBCConnectionHandler

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

class SQLServerConnectionHandler2 extends JDBCConnectionHandler {
  override val driverClassName: String = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
  lazy val host = SQLServerConfiguration.host.getOrElse(throw new IllegalArgumentException("SQLServer host not found in provided configuration."))
  lazy val port = SQLServerConfiguration.port.getOrElse(throw new IllegalArgumentException("SQLServer port not found in provided configuration."))
  lazy val user = SQLServerConfiguration.user.getOrElse(throw new IllegalArgumentException("SQLServer user not found in provided configuration."))
  lazy val password = SQLServerConfiguration.pass.getOrElse(throw new IllegalArgumentException("SQLServer password not found in provided configuration."))

  lazy val url = s"jdbc:sqlserver://$host:$port;databaseName=WSS_Content_DPGI;Instance=sql2012\\mssqlserver"
}
