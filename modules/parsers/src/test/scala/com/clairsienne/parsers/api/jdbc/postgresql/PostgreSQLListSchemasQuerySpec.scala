/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc.postgresql

import com.clairsienne.parsers.CLRPostgreSQLTestSpec

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class PostgreSQLListSchemasQuerySpec extends CLRPostgreSQLTestSpec {
  override implicit val patienceConfig = PatienceConfig(10.seconds)
  "PostgreSQLListSchemasQuery" should {
    val query = new PostgreSQLListSchemasQuery()
    "list databases on a PostgreSQL server" in {
      val schemas = query.execute().toSeq
      schemas.size should be > 0
      println(schemas.mkString("\n"))
      query.cleanup()
    }
  }
}
