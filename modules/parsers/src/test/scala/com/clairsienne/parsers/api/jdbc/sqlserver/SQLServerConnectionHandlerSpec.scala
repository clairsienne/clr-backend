/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.jdbc.sqlserver

import com.clairsienne.parsers.CLRTestSpec

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 14/01/2020
 */

class SQLServerConnectionHandlerSpec extends CLRTestSpec {
  override implicit val patienceConfig = PatienceConfig(10.seconds)
  "SQLServerConnectionHandler" should {
    val handler = new SQLServerConnectionHandler
    "load a configuration and open a connection to a SQL Server" in {
      val conn = handler.connect()
      conn.close()
    }
  }
}