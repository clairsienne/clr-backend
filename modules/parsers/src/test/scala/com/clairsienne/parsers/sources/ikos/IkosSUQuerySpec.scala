/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.sources.ikos

import akka.NotUsed
import akka.stream.scaladsl.{ Flow, Sink, Source }
import com.clairsienne.models.sources.ikos.SU
import com.clairsienne.parsers.{ CLRAS400TestSpec, CLRAkkaTestSpec }
import play.api.libs.json.Json

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-05
 */

class IkosSUQuerySpec extends CLRAS400TestSpec with CLRAkkaTestSpec {
  override implicit val patienceConfig = PatienceConfig(10.seconds)
  "IkosSUQuery" should {
    val query = new IkosSUQuery
    "properly execute query and map results to the given model" in {
      val src: Source[SU, NotUsed] = query.toSource()
      val f1 = Flow[SU].map { entity =>
        println(Json.prettyPrint(Json.toJson(entity)))
      }
      src.via(f1).runWith(Sink.ignore).futureValue
      query.cleanup()
    }
  }
}
