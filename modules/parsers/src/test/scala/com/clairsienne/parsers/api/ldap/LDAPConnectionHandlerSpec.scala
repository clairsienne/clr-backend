/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.parsers.api.ldap

import com.clairsienne.parsers.CLRAkkaTestSpec

import scala.concurrent.duration._

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 27/01/2020
 */

class LDAPConnectionHandlerSpec extends CLRAkkaTestSpec {
  override implicit val patienceConfig = PatienceConfig(10.seconds)
  "LDAPConnectionHandler" should {
    val handler = new LDAPConnectionHandler

    "load a configuration and open a connection to an LDAP server" in {

      //      handler.ldapWithBlockingPool.search("DC=logis,DC=local", "CN=De Rudder Carine", size=1).map { result =>
      //        result.headOption.map { entry =>
      //          val os = new ObjectOutputStream(new FileOutputStream("example.dat"))
      //          os.writeObject(entry)
      //          os.close()
      //          Assertions.succeed
      //        }.getOrElse(Assertions.fail)
      //      }
      //
      //      val future: Future[Seq[Entry]] = handler.ldapWithBlockingPool.search("DC=logis,DC=local", "CN=De Rudder Carine", size=1)
      //      future.onComplete{
      //        case Success(entries:Seq[Entry]) => {
      //          entries.foreach { entry: Entry =>
      //            println(entry)
      //          }
      //        }
      //        case Failure(err) => err.printStackTrace
      //      }
    }
  }

}
