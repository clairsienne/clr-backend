/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.sig

import java.nio.charset.StandardCharsets
import java.sql.ResultSet
import java.text.SimpleDateFormat

import com.clairsienne.models.rdf.skos.SkosConcept
import com.clairsienne.models.rdf.{ LocalizedLabel, RDFSerializer }
import com.clairsienne.models.sources.ikos.{ Operation, Residence }
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

case class Parcelle(
  primarykey: Option[String] = None,
  gid: Option[String] = None,
  codeinsee: Option[String] = None,
  commune: Option[String] = None,
  codeparcelle: Option[String] = None,
  destination: Option[String] = None,
  nature: Option[String] = None,
  identifiantresidence: Option[String] = None,
  identifiantoperation: Option[String] = None,
  superficie: Option[String] = None,
  dateacte: Option[String] = None,
  residence: Option[String] = None,
  caracterisation: Option[String] = None,
  wkt: Option[String] = None)

object Parcelle extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {

  implicit lazy val format = Json.format[Parcelle]

  def toJson(entity: Parcelle) = Json.toJsObject(entity)

  def pretty(entity: Parcelle) = Json.prettyPrint(toJson(entity))

  def hex2bytes(hex: String): Array[Byte] = {
    hex.replaceAll("[^0-9A-Fa-f]", "").sliding(2, 2).toArray.map(Integer.parseInt(_, 16).toByte)
  }

  def hex2string(hex: String): String = new String(hex2bytes(hex), StandardCharsets.UTF_8)

  def uri(id: String): String = s":parcel#${id}"

  def uri(entity: Parcelle): String = uri(entity.primarykey.get)

  def toModel(entity: Parcelle): Model = {

    import org.eclipse.rdf4j.model.vocabulary.{ DC, RDF }

    val df = new SimpleDateFormat("yyyy/MM/dd")

    val nature: Option[SkosConcept] = entity.nature.map(label => SkosConcept(LocalizedLabel(label)))
    val natureUri: Option[String] = nature.map(SkosConcept.uri(_))

    val destination: Option[SkosConcept] = entity.destination.map(label => SkosConcept(LocalizedLabel(label)))
    val destinationUri: Option[String] = destination.map(SkosConcept.uri(_))

    val skosmodel: Option[Model] = if (destination.isDefined) nature.map(m => RDFSerializer.merge(SkosConcept.toModel(m), SkosConcept.toModel(destination.get))) else nature.map(m => SkosConcept.toModel(m))

    val builder = RDFSerializer.getModel(skosmodel).subject(uri(entity)).add(RDF.TYPE, "clr:Parcel").add(RDF.TYPE, "mnx:Geometry")
    entity.gid.map(id => builder.add(RDFSerializer.createIRI("gid"), RDFSerializer.createLiteral(id)))
    entity.codeparcelle.map(code => builder.add(RDFSerializer.createIRI("codeParcelle"), RDFSerializer.createLiteral(code)))
    entity.destination.map(dest => builder.add(RDFSerializer.createIRI("destination"), RDFSerializer.createLiteral(dest)))
    entity.nature.map(nat => builder.add(RDFSerializer.createIRI("nature"), RDFSerializer.createLiteral(nat)))
    entity.caracterisation.map(carac => builder.add(RDFSerializer.createIRI("caracterisation"), RDFSerializer.createLiteral(carac)))
    entity.dateacte.map(d => builder.add(RDFSerializer.createIRI("dateActe"), RDFSerializer.createLiteral(df.parse(d))))
    entity.superficie.map(s => builder.add(RDFSerializer.createIRI("superficie"), RDFSerializer.createLiteral(s.toInt)))
    entity.identifiantoperation.map(id => builder.add(RDFSerializer.createIRI("hasOperation"), Operation.uri(id)))
    entity.identifiantresidence.map(id => builder.add(RDFSerializer.createIRI("hasResidence"), Residence.uri(id)))
    natureUri.map(id => builder.add(DC.SUBJECT, id))
    destinationUri.map(id => builder.add(DC.SUBJECT, id))
    //    entity.wkt.map{wkb =>
    // geometry in WKB format to be translated to WKT
    //      val aux:Array[Byte] = hex2bytes(wkb)
    //      val geom = new WKBReader().read(aux)
    //      builder.add(RDFSerializer.createMnxIRI("asWKT"), RDFSerializer.createLiteral(geom.toText()))
    //    }

    builder.build
  }

  def apply(resultSet: ResultSet): Parcelle = Parcelle(
    primarykey = Option(resultSet.getString("primarykey")),
    gid = Option(resultSet.getString("gid")),
    codeinsee = Option(resultSet.getString("codeinsee")),
    commune = Option(resultSet.getString("commune")),
    codeparcelle = Option(resultSet.getString("codeparcelle")),
    destination = Option(resultSet.getString("destination")),
    nature = Option(resultSet.getString("nature")),
    identifiantresidence = Option(resultSet.getString("identifiantresidence")),
    identifiantoperation = Option(resultSet.getString("identifiantoperation")),
    superficie = Option(resultSet.getString("superficie")),
    dateacte = Option(resultSet.getString("dateacte")).map(_.replace(" 00:00:00.000", "")),
    residence = Option(resultSet.getString("residence")),
    caracterisation = Option(resultSet.getString("caracterisation")),
    wkt = Option(resultSet.getString("wkt2")))
}
