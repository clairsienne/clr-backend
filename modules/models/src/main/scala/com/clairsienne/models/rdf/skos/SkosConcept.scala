/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.rdf.skos

import com.clairsienne.models.rdf.{ LocalizedLabel, RDFSerializer }
import org.eclipse.rdf4j.model.Model

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 10/03/2020
 */

case class SkosConcept(prefLabel: LocalizedLabel, broader: Option[SkosConcept] = None)

object SkosConcept {

  def uri(id: String): String = s":concept#${id}"

  def uri(entity: SkosConcept): String = uri(LocalizedLabel.md5(entity.prefLabel))

  def toModel(entity: SkosConcept): Model = {
    import org.eclipse.rdf4j.model.vocabulary.{ RDF, SKOS }

    RDFSerializer.getModel()
      .subject(uri(entity))
      .add(RDF.TYPE, "skos:Concept")
      .add(SKOS.PREF_LABEL, LocalizedLabel.toLiteral(entity.prefLabel))
      .build
  }
}