/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.sharepoint

import java.sql.ResultSet
import java.text.SimpleDateFormat

import com.clairsienne.models.rdf.skos.SkosConcept
import com.clairsienne.models.rdf.{ LocalizedLabel, RDFSerializer }
import com.clairsienne.models.sources.ikos.Residence
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

case class Convention(
  id: Option[String] = None,
  URLFichierSource: Option[String] = None,
  identifiantresidence: Option[String] = None,
  typeDossier: Option[String] = None,
  numAvenant: Option[String] = None,
  numConvention: Option[String] = None,
  dateSignature: Option[String] = None,
  dateExpiration: Option[String] = None,
  codepostal: Option[String] = None)

object Convention extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Convention]

  def toJson(entity: Convention) = Json.toJsObject(entity)

  def pretty(entity: Convention) = Json.prettyPrint(toJson(entity))

  def uri(id: String): String = s":convention#${id}"

  def uri(entity: Convention): String = uri(entity.id.get)

  def toModel(entity: Convention): Model = {

    import org.eclipse.rdf4j.model.vocabulary.{ DC, RDF }

    val df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

    val skosConcept: Option[SkosConcept] = entity.typeDossier.map(label => SkosConcept(LocalizedLabel(label.capitalize)))
    val skosConceptUri: Option[String] = skosConcept.map(SkosConcept.uri(_))
    val builder = RDFSerializer.getModel(skosConcept.map(SkosConcept.toModel(_)))

    builder.subject(uri(entity)).add(RDF.TYPE, "clr:ConventionLocative")

    entity.URLFichierSource.map(url => builder.add(RDFSerializer.createIRI("locatedAt"), RDFSerializer.createLiteral(url)))
    entity.identifiantresidence.map(id => builder.add(RDFSerializer.createIRI("hasResidence"), Residence.uri(id)))
    entity.numConvention.map(num => builder.add(RDFSerializer.createIRI("numConvention"), RDFSerializer.createLiteral(num)))
    entity.numAvenant.map(num => builder.add(RDFSerializer.createIRI("numAvenant"), RDFSerializer.createLiteral(num)))
    entity.dateExpiration.map(t => builder.add(RDFSerializer.createIRI("dateExpiration"), RDFSerializer.createLiteral(df.parse(t))))
    entity.dateSignature.map(t => builder.add(RDFSerializer.createIRI("dateSignature"), RDFSerializer.createLiteral(df.parse(t))))
    skosConceptUri.map(id => builder.add(DC.SUBJECT, id))

    builder.build

  }

  def apply(resultSet: ResultSet): Convention = Convention(
    id = Option(resultSet.getString("Id")),
    URLFichierSource = Option(resultSet.getString("URLFichierSource")),
    identifiantresidence = Option(resultSet.getString("identifiantresidence")),
    typeDossier = Option(resultSet.getString("typeDossier")),
    numAvenant = Option(resultSet.getString("numAvenant")),
    numConvention = Option(resultSet.getString("numConvention")),
    dateSignature = Option(resultSet.getString("dateSignature")),
    dateExpiration = Option(resultSet.getString("dateExpiration")),
    codepostal = Option(resultSet.getString("codepostal")))

}