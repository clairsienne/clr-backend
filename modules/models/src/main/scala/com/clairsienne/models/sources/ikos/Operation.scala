/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.rdf.RDFSerializer
import com.clairsienne.models.rdf.foaf.FoafOrganization
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

case class Operation(
  idoperation: String,
  identifiantorganisme: String,
  identifiantoperation: String,
  codeoperation: String,
  numerotranche: String,
  nomoperation: String,
  natureoperation: String,
  identifiantresidence: String)

object Operation extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Operation]

  def toJson(entity: Operation) = Json.toJsObject(entity)

  def pretty(entity: Operation) = Json.prettyPrint(toJson(entity))

  def uri(id: String): String = s":operation#${id}"

  def uri(entity: Operation): String = uri(entity.idoperation)

  def toModel(entity: Operation): Model = {
    import org.eclipse.rdf4j.model.vocabulary.RDF
    import org.eclipse.rdf4j.model.vocabulary.RDFS

    RDFSerializer.getModel()
      .subject(uri(entity))
      .add(RDF.TYPE, "clr:Operation")
      .add(RDFSerializer.createIRI("hasOrg"), FoafOrganization.uri(entity.identifiantorganisme))
      .add(RDFSerializer.createIRI("hasResidence"), Residence.uri(entity.identifiantresidence))
      .add(RDFSerializer.createIRI("identifiantOperation"), RDFSerializer.createLiteral(entity.identifiantoperation))
      .add(RDFS.LABEL, RDFSerializer.createLiteral(entity.nomoperation))
      .add(RDFSerializer.createIRI("numeroTranche"), RDFSerializer.createLiteral(entity.numerotranche))
      .add(RDFSerializer.createIRI("codeOperation"), RDFSerializer.createLiteral(entity.codeoperation))
      .add(RDFSerializer.createIRI("hasOperationNature"), RDFSerializer.createLiteral(entity.natureoperation))
      .build
  }

  def apply(resultSet: ResultSet): Operation = Operation(
    idoperation = resultSet.getString("idoperation").trim,
    identifiantorganisme = resultSet.getString("identifiantorganisme").trim,
    identifiantoperation = resultSet.getString("identifiantoperation").trim,
    codeoperation = resultSet.getString("codeoperation").trim,
    numerotranche = resultSet.getString("numerotranche").trim,
    nomoperation = resultSet.getString("nomoperation").trim,
    natureoperation = resultSet.getString("natureoperation").trim,
    identifiantresidence = resultSet.getString("identifiantresidence").trim)
}

