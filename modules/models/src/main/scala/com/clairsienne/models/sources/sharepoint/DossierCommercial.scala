/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.sharepoint

import java.sql.ResultSet
import java.text.SimpleDateFormat

import com.clairsienne.models.rdf.RDFSerializer
import com.clairsienne.models.sources.ikos.Residence
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

case class DossierCommercial(
  id: Option[String] = None,
  URLFichierSource: Option[String] = None,
  TimeCreated: Option[String] = None,
  TimeLastModified: Option[String] = None,
  identifiantresidence: Option[String] = None,
  codepostal: Option[String] = None)

object DossierCommercial extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {

  implicit lazy val format = Json.format[DossierCommercial]

  def toJson(entity: DossierCommercial) = Json.toJsObject(entity)

  def pretty(entity: DossierCommercial) = Json.prettyPrint(toJson(entity))

  def uri(id: String): String = s":dcomm#${id}"

  def uri(entity: DossierCommercial): String = uri(entity.id.get)

  def toModel(entity: DossierCommercial): Model = {

    import org.eclipse.rdf4j.model.vocabulary.RDF

    val df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

    val builder = RDFSerializer.getModel()

    builder.subject(uri(entity)).add(RDF.TYPE, "clr:DossierCommercial")

    entity.URLFichierSource.map(url => builder.add(RDFSerializer.createIRI("locatedAt"), RDFSerializer.createLiteral(url)))
    entity.identifiantresidence.map(id => builder.add(RDFSerializer.createIRI("hasResidence"), Residence.uri(id)))
    entity.TimeCreated.map(t => builder.add(RDFSerializer.createIRI("createdAt"), RDFSerializer.createLiteral(df.parse(t))))
    entity.TimeLastModified.map(t => builder.add(RDFSerializer.createIRI("lastUpdate"), RDFSerializer.createLiteral(df.parse(t))))

    builder.build

  }

  def apply(resultSet: ResultSet): DossierCommercial = DossierCommercial(
    id = Option(resultSet.getString("id")),
    URLFichierSource = Option(resultSet.getString("URLFichierSource")),
    TimeCreated = Option(resultSet.getString("TimeCreated")),
    TimeLastModified = Option(resultSet.getString("TimeLastModified")),
    identifiantresidence = Option(resultSet.getString("identifiantresidence")),
    codepostal = Option(resultSet.getString("codepostal")))

}