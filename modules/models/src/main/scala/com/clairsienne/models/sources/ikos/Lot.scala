/**
  * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
  * and other contributors as indicated by the @author tags.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package com.clairsienne.models.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.rdf.{LocalizedLabel, RDFSerializer}
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
  * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
  * Date: 2019-10-21
  */

case class Lot(
  identifiantlot: String,
  identifiantorganisme: String,
  identifiantresidence: String,
  identifiantbatiment: String,
  identifiantentree: String,
  niveaubatiment: Int,
  numeroPorte: String,
  typehabitation: String,
  typologielot: String,
  modefinancement: String,
  naturelot: String,
  adresse:Adresse,
  lotenfindegestion: String,
  modeGestion: String
)

object Lot extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Lot]

  def toJson(entity: Lot) = Json.toJsObject(entity)

  def pretty(entity: Lot) = Json.prettyPrint(toJson(entity))

  def uri(id: String): String = s":lot#${id}"

  def uri(entity: Lot): String = uri(entity.identifiantlot)

  def toModel(entity: Lot):Model = {
    import org.eclipse.rdf4j.model.vocabulary.RDF

    RDFSerializer.getModel(Some(Adresse.toModel(entity.adresse)))
      .subject(uri(entity))
      .add(RDF.TYPE, "clr:Lot")
      .add(RDFSerializer.createIRI("hasBuilding"), Batiment.uri(entity.identifiantbatiment))
      .add(RDFSerializer.createIRI("hasLotNature"), RDFSerializer.createLiteral(entity.naturelot))
      .add(RDFSerializer.createIRI("hasLotType"), RDFSerializer.createLiteral(entity.typologielot))
      .add(RDFSerializer.createIRI("hasFundingMode"), RDFSerializer.createLiteral(entity.modefinancement))
      .add(RDFSerializer.createIRI("hasGestionMode"), RDFSerializer.createLiteral(entity.modeGestion))
      .add(RDFSerializer.createIRI("doorNumber"), RDFSerializer.createLiteral(entity.numeroPorte))
      .add(RDFSerializer.createIRI("storey"), RDFSerializer.createLiteral(entity.niveaubatiment))
      .add(RDFSerializer.createMnxIRI("hasAddress"), Adresse.uri(entity.adresse))
      .build
  }

  def apply(resultSet: ResultSet): Lot = Lot(
    identifiantlot = resultSet.getString("identifiantlot").trim,
    identifiantorganisme = resultSet.getString("identifiantorganisme").trim,
    identifiantresidence = resultSet.getString("identifiantresidence").trim,
    identifiantbatiment = resultSet.getString("identifiantbatiment").trim,
    identifiantentree = resultSet.getString("identifiantentree").trim,
    niveaubatiment = resultSet.getInt("niveaubatiment"),
    numeroPorte = resultSet.getString("numeroPorte").trim,
    typehabitation = resultSet.getString("typehabitation").trim,
    typologielot = resultSet.getString("typologielot").trim,
    modefinancement = resultSet.getString("modefinancement").trim,
    naturelot = resultSet.getString("naturelot").trim,
    Adresse(
      adresse1 = resultSet.getString("adresse").trim,
      adresse2 = Option(resultSet.getString("adressesuite")).map(_.trim),
      commune = resultSet.getString("commune").trim,
      epci = resultSet.getString("epci").trim,
      codedepartement = resultSet.getString("codedepartement").trim,
      codeinsee = resultSet.getString("codeinsee").trim,
      codepostal = resultSet.getString("codepostal").trim,
    ),
    lotenfindegestion = resultSet.getString("lotenfindegestion").trim,
    modeGestion = resultSet.getString("modeGestion").trim
  )
}


