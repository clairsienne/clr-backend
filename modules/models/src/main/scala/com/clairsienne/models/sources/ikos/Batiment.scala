/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.rdf.RDFSerializer
import com.clairsienne.models.rdf.foaf.FoafOrganization
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

case class Batiment(
  idbatiment: String,
  identifiantorganisme: String,
  identifiantresidence: String,
  identifiantbatiment: String,
  identifiantentree: String,
  adresse: Adresse,
  typehabitation: String
)

object Batiment extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Batiment]

  def toJson(entity: Batiment) = Json.toJsObject(entity)

  def uri(id: String): String = s":building#${id}"

  def uri(entity: Batiment): String = uri(entity.idbatiment)

  def toModel(entity: Batiment):Model = {
    import org.eclipse.rdf4j.model.vocabulary.RDF

    RDFSerializer.getModel(Some(Adresse.toModel(entity.adresse)))
      .subject(uri(entity))
      .add(RDF.TYPE, "clr:Building")
      .add(RDFSerializer.createIRI("hasOrg"), FoafOrganization.uri(entity.identifiantorganisme))
      .add(RDFSerializer.createIRI("hasResidence"), Residence.uri(entity.identifiantresidence))
      .subject(Entrance.uri(entity.identifiantentree))
      .add(RDF.TYPE, "clr:Entrance")
      .add(RDFSerializer.createIRI("isEntranceOf"), Batiment.uri(entity.idbatiment))
      .add(RDFSerializer.createMnxIRI("hasAddress"), Adresse.uri(entity.adresse))
      .build
  }

//  def toModel(resultSet: ResultSet):Model = toModel(Batiment(resultSet))
//  def toJsonLd(resultSet: ResultSet) = RDFSerializer.toJsonLd(toModel(resultSet))
//  def toTriG(resultSet: ResultSet) = RDFSerializer.toTriG(toModel(resultSet))
//  def pretty(bat: Batiment) = Json.prettyPrint(toJson(bat))

  def apply(resultSet: ResultSet): Batiment = {
    Batiment(
      idbatiment = resultSet.getString("idbatiment").trim,
      identifiantorganisme = resultSet.getString("identifiantorganisme").trim,
      identifiantresidence = resultSet.getString("identifiantresidence").trim,
      identifiantbatiment = resultSet.getString("identifiantbatiment").trim,
      identifiantentree = resultSet.getString("identifiantentree").trim,
      Adresse(
        adresse1 = resultSet.getString("adresseresidence").trim,
        adresse2 = Option(resultSet.getString("adressesuite")).map(_.trim),
        commune = resultSet.getString("commune").trim,
        epci = resultSet.getString("epci").trim,
        codedepartement = resultSet.getString("codedepartement").trim,
        codeinsee = resultSet.getString("codeinsee").trim,
        codepostal = resultSet.getString("codepostal").trim,
      ),
      typehabitation = resultSet.getString("typehabitation").trim
    )
  }
}


