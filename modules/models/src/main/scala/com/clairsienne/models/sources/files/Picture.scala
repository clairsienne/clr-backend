/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.files

import com.clairsienne.models.rdf.RDFSerializer
import com.clairsienne.models.sources.ikos.Residence
import org.eclipse.rdf4j.model.Model
import org.eclipse.rdf4j.model.vocabulary.{FOAF, RDF, RDFS}
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 08/07/2020
 */

case class Picture(
  filename: String,
  uri: String,
  idResidence: String,
  pictureType: String,
  isExt: Boolean = true)

object Picture extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {

  implicit lazy val format = Json.format[Picture]

  def toJson(entity: Picture) = Json.toJsObject(entity)

  def pretty(entity: Picture) = Json.prettyPrint(toJson(entity))

  def uri(entity: Picture): String = s"https://data.clairsienne.fr:3334${entity.uri}"

  def toModel(entity: Picture): Model = {

    RDFSerializer.getModel()
      .subject(uri(entity))
      .add(RDF.TYPE, FOAF.IMAGE)
      .add(RDFS.LABEL, entity.filename)
      .add(RDFSerializer.createIRI("hasResidence"), Residence.uri(entity.idResidence))
      .add(RDFSerializer.createIRI("pictureType"), entity.pictureType)
      .add(RDFSerializer.createIRI("isExt"), entity.isExt)
      .build
  }

}