/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.ikos

import com.clairsienne.models.rdf.RDFSerializer
import com.clairsienne.models.utils.CryptoUtils
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-11-06
 */

case class Adresse(
  adresse1: String,
  adresse2: Option[String] = None,
  commune: String,
  epci: String,
  codedepartement: String,
  codeinsee: String,
  codepostal: String,
  latitude: Option[BigDecimal] = None,
  longitude: Option[BigDecimal] = None)

object Adresse extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Adresse]

  def toJson(entity: Adresse) = Json.toJsObject(entity)

  def pretty(entity: Adresse) = Json.prettyPrint(toJson(entity))

  def md5(entity: Adresse) = CryptoUtils.md5sum(
    entity.adresse1,
    entity.adresse2.getOrElse(""),
    entity.commune,
    entity.epci,
    entity.codedepartement,
    entity.codeinsee,
    entity.codepostal)

  def uri(entity: Adresse): String = {
    val addrId = md5(entity)
    s":address#${addrId}"
  }

  def toModel(entity: Adresse): Model = {

    import org.eclipse.rdf4j.model.vocabulary.RDF

    var builder = RDFSerializer.getModel()
      .subject(uri(entity))
      .add(RDF.TYPE, "mnx:Address")
      .add(RDFSerializer.createMnxIRI("street1"), RDFSerializer.createLiteral(entity.adresse1))
      .add(RDFSerializer.createMnxIRI("city"), RDFSerializer.createLiteral(entity.commune))
      .add(RDFSerializer.createMnxIRI("postalCode"), RDFSerializer.createLiteral(entity.codepostal))
      .add(RDFSerializer.createIRI("insee"), RDFSerializer.createLiteral(entity.codeinsee))
      .add(RDFSerializer.createIRI("deptCode"), RDFSerializer.createLiteral(entity.codedepartement))
      .add(RDFSerializer.createIRI("epci"), RDFSerializer.createLiteral(entity.epci))

    builder = if (entity.adresse2.isDefined) builder.add(RDFSerializer.createMnxIRI("street2"), RDFSerializer.createLiteral(entity.adresse2.get)) else builder
    builder.build
  }

  def toJsonLd(entity: Adresse) = RDFSerializer.toJsonLd(toModel(entity))

}