/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.sharepoint

import java.sql.ResultSet
import java.text.SimpleDateFormat

import com.clairsienne.models.rdf.skos.SkosConcept
import com.clairsienne.models.rdf.{ LocalizedLabel, RDFSerializer }
import com.clairsienne.models.sources.ikos.{ Operation, Residence }
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2020-01-10
 */

case class ActeAchat(
  id: Option[String] = None,
  URLFichierSource: Option[String] = None,
  TimeCreated: Option[String] = None,
  TimeLastModified: Option[String] = None,
  identifiantresidence: Option[String] = None,
  identifiantoperation: Option[String] = None,
  nomresidence: Option[String] = None,
  typeActe: Option[String] = None,
  tiersvendeur: Option[String] = None,
  dateSignature: Option[String] = None,
  commune: Option[String] = None)

object ActeAchat extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {

  implicit lazy val format = Json.format[ActeAchat]

  def toJson(entity: ActeAchat) = Json.toJsObject(entity)

  def pretty(entity: ActeAchat) = Json.prettyPrint(toJson(entity))

  def uri(id: String): String = s":acte#${id}"

  def uri(entity: ActeAchat): String = uri(entity.id.get)

  def toModel(entity: ActeAchat): Model = {

    import org.eclipse.rdf4j.model.vocabulary.{ DC, RDF }

    val df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

    val skosConcept: Option[SkosConcept] = entity.typeActe.map(label => SkosConcept(LocalizedLabel(label)))
    val skosConceptUri: Option[String] = skosConcept.map(SkosConcept.uri(_))

    val builder = RDFSerializer.getModel(skosConcept.map(SkosConcept.toModel(_)))

    builder.subject(uri(entity)).add(RDF.TYPE, "clr:ActeAuthentique")

    entity.URLFichierSource.map(url => builder.add(RDFSerializer.createIRI("locatedAt"), RDFSerializer.createLiteral(url)))
    entity.TimeCreated.map(t => builder.add(RDFSerializer.createIRI("createdAt"), RDFSerializer.createLiteral(df.parse(t))))
    entity.TimeLastModified.map(t => builder.add(RDFSerializer.createIRI("lastUpdate"), RDFSerializer.createLiteral(df.parse(t))))
    entity.dateSignature.map(t => builder.add(RDFSerializer.createIRI("dateSignature"), RDFSerializer.createLiteral(df.parse(t))))
    entity.tiersvendeur.map(t => builder.add(RDFSerializer.createIRI("tiersVendeur"), RDFSerializer.createLiteral(t)))
    skosConceptUri.map(id => builder.add(DC.SUBJECT, id))
    entity.identifiantoperation.map(id => builder.add(RDFSerializer.createIRI("hasOperation"), Operation.uri(id)))
    entity.identifiantresidence.map(id => builder.add(RDFSerializer.createIRI("hasResidence"), Residence.uri(id)))

    builder.build
  }

  def apply(resultSet: ResultSet): ActeAchat = {
    ActeAchat(
      id = Option(resultSet.getString("Id")),
      URLFichierSource = Option(resultSet.getString("URLFichierSource")),
      TimeCreated = Option(resultSet.getString("TimeCreated")),
      TimeLastModified = Option(resultSet.getString("TimeLastModified")),
      identifiantresidence = Option(resultSet.getString("identifiantresidence")),
      identifiantoperation = Option(resultSet.getString("identifiantoperation")),
      nomresidence = Option(resultSet.getString("nomresidence")),
      typeActe = Option(resultSet.getString("typeActe")),
      tiersvendeur = Option(resultSet.getString("tiersvendeur")),
      dateSignature = Option(resultSet.getString("dateSignature")),
      commune = Option(resultSet.getString("commune")))
  }
}
