/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.ikos

import java.sql.ResultSet

import com.clairsienne.models.rdf.RDFSerializer
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

case class SHA(identifiantlot: String, value: String)

object SHA extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[SHA]

  def toJson(entity: SHA) = Json.toJsObject(entity)

  def pretty(entity: SHA) = Json.prettyPrint(toJson(entity))

  def toModel(entity: SHA): Model = {
    RDFSerializer.getModel()
      .subject(Lot.uri(entity.identifiantlot))
      .add(RDFSerializer.createIRI("SHA"), RDFSerializer.createLiteral(entity.value))
      .build
  }

  def apply(resultSet: ResultSet): SHA = SHA(
    identifiantlot = resultSet.getString("identifiantlot").trim,
    value = resultSet.getString("SHA").trim)
}
