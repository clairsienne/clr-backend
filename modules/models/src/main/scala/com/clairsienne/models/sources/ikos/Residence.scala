/**
 * Copyright (C) 2013-2019 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.clairsienne.models.sources.ikos

import java.sql.{ Date, ResultSet }

import com.clairsienne.models.rdf.RDFSerializer
import org.eclipse.rdf4j.model.Model
import play.api.libs.json.Json

/**
 * Created by Nicolas DELAFORGE (nicolas.delaforge@mnemotix.com).
 * Date: 2019-10-21
 */

case class Residence(
  identifiantresidence: String,
  identifiantorganisme: String,
  nomresidence: Option[String],
  typehabitation: String,
  dateacquisition: Option[Date] = None,
  dateconstruction: Option[Date] = None,
  datemiseensercice: Option[Date] = None,
  dateLivraison: Option[Date] = None,
  datereceptiontravaux: Option[Date] = None,
  modeAcquisition: String,
  modeGestion: String,
  antenne: Option[String],
  adresse: Adresse,
  nblogementinitial: Int,
  nblogementgere: Int,
  nblogementcollectif: Int,
  nblogementindividuel: Int,
  nbannexe: Int,
  nbdemolis: Int,
  nbvendus: Int)

object Residence extends de.heikoseeberger.akkahttpplayjson.PlayJsonSupport with spray.json.DefaultJsonProtocol {
  implicit lazy val format = Json.format[Residence]

  def toJson(entity: Residence) = Json.toJsObject(entity)

  def pretty(entity: Residence) = Json.prettyPrint(toJson(entity))

  def uri(id: String): String = s":residence#${id}"

  def uri(entity: Residence): String = uri(entity.identifiantresidence)

  def toModel(entity: Residence): Model = {
    import org.eclipse.rdf4j.model.vocabulary.{ RDF, RDFS }

    var builder = RDFSerializer.getModel(Some(Adresse.toModel(entity.adresse)))
      .subject(uri(entity))
      .add(RDF.TYPE, "clr:Residence")
      .add(RDFSerializer.createIRI("hasHabitationType"), RDFSerializer.createLiteral(entity.typehabitation))
      .add(RDFSerializer.createIRI("hasAcquisitionMode"), RDFSerializer.createLiteral(entity.modeAcquisition))
      .add(RDFSerializer.createIRI("hasGestionMode"), RDFSerializer.createLiteral(entity.modeGestion))

      .add(RDFSerializer.createIRI("nbLogementsInit"), RDFSerializer.createLiteral(entity.nblogementinitial))
      .add(RDFSerializer.createIRI("nbLogementsGeres"), RDFSerializer.createLiteral(entity.nblogementgere))
      .add(RDFSerializer.createIRI("nbLogementsCollectifs"), RDFSerializer.createLiteral(entity.nblogementcollectif))
      .add(RDFSerializer.createIRI("nbLogementsIndividuels"), RDFSerializer.createLiteral(entity.nblogementindividuel))
      .add(RDFSerializer.createIRI("nbAnnexes"), RDFSerializer.createLiteral(entity.nbannexe))
      .add(RDFSerializer.createIRI("nbDemolis"), RDFSerializer.createLiteral(entity.nbdemolis))
      .add(RDFSerializer.createIRI("nbVendus"), RDFSerializer.createLiteral(entity.nbvendus))
      .add(RDFSerializer.createMnxIRI("hasAddress"), Adresse.uri(entity.adresse))

    builder = if (entity.nomresidence.isDefined) builder.add(RDFS.LABEL, RDFSerializer.createLiteral(entity.nomresidence.get)) else builder
    builder = if (entity.antenne.isDefined) builder.add(RDFSerializer.createIRI("antenne"), RDFSerializer.createLiteral(entity.antenne.get)) else builder
    builder = if (entity.dateacquisition.isDefined) builder.add(RDFSerializer.createIRI("dateAcquisition"), RDFSerializer.createLiteral(entity.dateacquisition.get)) else builder
    builder = if (entity.datereceptiontravaux.isDefined) builder.add(RDFSerializer.createIRI("dateReceptionTravaux"), RDFSerializer.createLiteral(entity.datereceptiontravaux.get)) else builder
    builder = if (entity.dateLivraison.isDefined) builder.add(RDFSerializer.createIRI("dateLivraison"), RDFSerializer.createLiteral(entity.dateLivraison.get)) else builder
    builder = if (entity.dateconstruction.isDefined) builder.add(RDFSerializer.createIRI("dateConstruction"), RDFSerializer.createLiteral(entity.dateconstruction.get)) else builder
    builder = if (entity.datemiseensercice.isDefined) builder.add(RDFSerializer.createIRI("dateMiseEnService"), RDFSerializer.createLiteral(entity.datemiseensercice.get)) else builder

    builder.build
  }

  def apply(resultSet: ResultSet): Residence = {
    Residence(
      identifiantresidence = resultSet.getString("identifiantresidence"),
      identifiantorganisme = resultSet.getString("identifiantorganisme"),
      nomresidence = Option(resultSet.getString("nomresidence")).map(_.trim),
      typehabitation = resultSet.getString("typehabitation"),
      dateacquisition = Option(resultSet.getDate("dateacquisition")),
      dateconstruction = Option(resultSet.getDate("dateconstruction")),
      datemiseensercice = Option(resultSet.getDate("datemiseensercice")),
      dateLivraison = Option(resultSet.getDate("dateLivraison")),
      datereceptiontravaux = Option(resultSet.getDate("datereceptiontravaux")),
      modeAcquisition = resultSet.getString("modeAcquisition").trim,
      modeGestion = resultSet.getString("modeGestion"),
      antenne = Option(resultSet.getString("antenne")),
      adresse = Adresse(
        adresse1 = resultSet.getString("adresse").trim,
        adresse2 = Option(resultSet.getString("adressesuite")).map(_.trim),
        commune = resultSet.getString("commune").trim,
        epci = resultSet.getString("epci").trim,
        codedepartement = resultSet.getString("codedepartement"),
        codeinsee = resultSet.getString("codeinsee"),
        codepostal = resultSet.getString("codepostal"),
        latitude = Option(resultSet.getBigDecimal("latitude")),
        longitude = Option(resultSet.getBigDecimal("longitude"))),
      nblogementinitial = resultSet.getInt("nblogementinitial"),
      nblogementgere = resultSet.getInt("nblogementgere"),
      nblogementcollectif = resultSet.getInt("nblogementcollectif"),
      nblogementindividuel = resultSet.getInt("nblogementindividuel"),
      nbannexe = resultSet.getInt("nbannexe"),
      nbdemolis = resultSet.getInt("nbdemolis"),
      nbvendus = resultSet.getInt("nbvendus"))
  }
}
