import sbt._

object Versions {
  lazy val akkaHttpVersion = "10.1.10"
  lazy val akkaVersion = "2.5.25"
  lazy val poi = "4.1.0"
  lazy val rdf4jVersion = "2.5.0"
  lazy val scalatest = "3.0.8"
  lazy val jt400 = "9.8"
  lazy val sqlJdbc42 = "6.1.0.jre8"
  lazy val postgresql = "9.4.1212"
  lazy val ldap = "0.4.1"
}

object Dependencies {
  lazy val quartz = "org.quartz-scheduler" % "quartz" % "2.3.2"

  // Akka
  lazy val akkaStream = "com.typesafe.akka" %% "akka-stream" % Versions.akkaVersion
  lazy val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % Versions.akkaVersion
  lazy val akkaHttp = "com.typesafe.akka" %% "akka-http" % Versions.akkaHttpVersion
  lazy val akkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % Versions.akkaHttpVersion
  lazy val akkaHttpXml = "com.typesafe.akka" %% "akka-http-xml" % Versions.akkaHttpVersion
  lazy val akkaHttpPlayJson = "de.heikoseeberger" %% "akka-http-play-json" % "1.29.1"
  // Test
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.8"
  lazy val akkaHttpTestKit = "com.typesafe.akka" %% "akka-http-testkit" % Versions.akkaHttpVersion
  lazy val akkaTestkit = "com.typesafe.akka" %% "akka-testkit" % Versions.akkaVersion
  lazy val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % Versions.akkaVersion

  lazy val jodaTime = "joda-time" % "joda-time" % "2.10.1"
  lazy val typesafeConfig = "com.typesafe" % "config" % "1.4.0"
  lazy val playJson = "com.typesafe.play" %% "play-json" % "2.7.0"
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % "1.2.3"

  // Apache POI
  lazy val poi = "org.apache.poi" % "poi" % Versions.poi
  lazy val poiXml = "org.apache.poi" % "poi-ooxml" % Versions.poi
  lazy val poiXmlSchemas = "org.apache.poi" % "poi-ooxml-schemas" % Versions.poi

  lazy val jt400 = "net.sf.jt400" % "jt400" % Versions.jt400
  lazy val postgresql = "org.postgresql" % "postgresql" % Versions.postgresql
  lazy val sqlJdbc42 = "com.microsoft.sqlserver" % "mssql-jdbc" % Versions.sqlJdbc42

  lazy val rdf4jRuntime = "org.eclipse.rdf4j" % "rdf4j-runtime" % Versions.rdf4jVersion

  lazy val sttp = "com.softwaremill.sttp.client" %% "core" % "2.1.1"

  //  lazy val ldaptive = "org.ldaptive" % "ldaptive" % "2.0.0-RC1"
  lazy val ldap = "pt.tecnico.dsi" %% "ldap" % Versions.ldap excludeAll(
    ExclusionRule("com.typesafe.scala-logging", "scala-logging"),
    ExclusionRule("ch.qos.logback", "logback-classic"),
    ExclusionRule("org.scalatest", "scalatest"),
    ExclusionRule("com.typesafe", "config")
  )
}
