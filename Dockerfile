FROM openjdk:8-jre-alpine
ADD target/scala-2.12/clr-backend.jar clr-backend.jar
ENTRYPOINT ["java", "-jar", "clr-backend.jar"]